# -*- coding: utf-8 -*-

"""Top-level package for Click Pomodoro."""

__author__ = """Emanuel Calso"""
__email__ = 'eman@bloodpet.com'
__version__ = '0.1.0'
