from dataclasses import dataclass
from datetime import datetime, timedelta
from typing import Any, List, Optional


class TimedBase(object):
    end_dt: Any = None

    def __init__(self) -> None:
        self.start_dt: datetime = datetime.now()

    def __repr__(self) -> str:
        return f'{self.__class__}() start_dt={self.start_dt} end_dt={self.end_dt}'

    @property
    def duration(self) -> timedelta:
        if self.end_dt is None:
            end_dt = datetime.now()
        else:
            end_dt = self.end_dt
        return end_dt - self.start_dt

    @property
    def has_ended(self) -> bool:
        return self.end_dt is not None

    @property
    def seconds(self) -> float:
        return self.duration.seconds

    def end(self) -> object:
        if not self.has_ended:
            self.end_dt = datetime.now()
        return self


class Pause(TimedBase):
    pass


class Activity(TimedBase):
    def __init__(self, stage: str) -> None:
        super().__init__()
        self.pauses: List[Pause] = []
        self.stage: str = stage

    @property
    def duration(self) -> timedelta:
        td = super().duration
        pause_total = sum((pause.duration for pause in self.pauses), timedelta(0))
        return td - pause_total

    @property
    def is_paused(self) -> bool:
        if not self.pauses:
            return False
        pause = self.pauses[-1]
        return not pause.has_ended

    def pause(self) -> object:
        if not self.pauses or not self.is_paused:
            self.pauses.append(Pause())
        return self

    def unpause(self) -> object:
        if self.is_paused:
            self.pauses[-1].end()
        return self


@dataclass
class Pomodoro:
    def __init__(self) -> None:
        self.activity: Activity = Activity('work')
        self.work_sessions: List[Activity] = [self.activity]
        self.short_breaks: List[Activity] = []
        self.long_break: Optional[Activity] = None
        self.config: dict = {
            'work': 25,
            'short_break': 5,
            'long_break': 15,
        }

    @property
    def stage(self):
        return self.activity.stage

    def next(self) -> object:
        self.activity.end()
        if self.stage == 'work':
            if len(self.work_sessions) < 4:
                self.activity = Activity('short_break')
                self.short_breaks.append(self.activity)
            else:
                self.long_break = self.activity = Activity('long_break')
        else:
            self.activity = Activity('work')
            self.work_sessions.append(self.activity)
        return self

    def pause(self) -> object:
        self.activity.pause()
        return self

    def unpause(self) -> object:
        self.activity.unpause()
        return self

    @property
    def has_ended(self):
        if self.long_break is None:
            return False
        return self.long_break.has_ended

    def end(self):
        if self.long_break is None:
            raise ValueError('Cannot end w/o a long break')
        self.activity.end()

    @property
    def expected_seconds(self):
        return self.config[self.stage] * 60

    @property
    def expected_duration(self):
        return timedelta(seconds=self.expected_seconds)

    @property
    def remaining_duration(self):
        return self.expected_duration - self.activity.duration

    @property
    def remaining_seconds(self):
        return self.expected_seconds - self.activity.seconds
