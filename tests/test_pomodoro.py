from datetime import datetime, timedelta
import random

import pytest

from clickmodoro import pomodoro


def test_timebase_duration():
    expected_seconds = random.randint(1, 86400)
    expected_td = timedelta(seconds=expected_seconds)
    timebase = pomodoro.TimedBase()
    now = datetime.now()
    timebase.start_dt = now - expected_td
    assert expected_td.seconds == timebase.duration.seconds
    timebase.end_dt = now
    assert expected_td.seconds == timebase.duration.seconds
    assert expected_seconds == timebase.seconds


def test_timebase_end():
    timebase = pomodoro.TimedBase()
    assert not timebase.has_ended
    timebase.end_dt = datetime.now()
    assert timebase.has_ended
    timebase = pomodoro.TimedBase()
    timebase.end()
    assert timebase.has_ended


def test_activity_duration():
    expected_seconds = random.randint(1, 86400)
    expected_td = timedelta(seconds=expected_seconds)
    activity = pomodoro.Activity('test')
    assert activity.duration
    activity.end_dt = datetime.now()
    activity.start_dt = activity.end_dt - expected_td
    assert expected_td == activity.duration
    assert expected_seconds == activity.seconds
    paused_seconds = random.randint(1, 300)
    paused_td = timedelta(seconds=paused_seconds)
    pause = pomodoro.Pause()
    pause.end_dt = activity.end_dt
    pause.start_dt = pause.end_dt - paused_td
    activity.pauses = [pause]
    assert expected_td - paused_td == activity.duration
    assert expected_seconds - paused_seconds == activity.seconds


def test_activity_pause():
    activity = pomodoro.Activity('test')
    assert not activity.is_paused
    pause = pomodoro.Pause()
    activity.pauses = [pause]
    assert activity.is_paused
    pause.end_dt = datetime.now()
    assert not activity.is_paused
    activity.pause()
    assert activity.is_paused
    activity.unpause()
    assert not activity.is_paused


def test_pomodoro():
    pom = pomodoro.Pomodoro()
    assert 'work' == pom.stage
    assert 25 * 60 == pom.remaining_seconds
    pom.next()
    assert 'short_break' == pom.stage
    assert 5 * 60 == pom.remaining_seconds
    pom.next()
    assert 'work' == pom.stage
    pom.next()
    assert 'short_break' == pom.stage
    pom.next()
    assert 'work' == pom.stage
    pom.next()
    assert 'short_break' == pom.stage
    pom.next()
    assert 'work' == pom.stage
    assert not pom.has_ended
    with pytest.raises(ValueError):
        pom.end()
    pom.next()
    assert 'long_break' == pom.stage
    assert 15 * 60 == pom.remaining_seconds
    assert not pom.has_ended
    pom.end()
    assert pom.has_ended
