==============
Click Pomodoro
==============


.. image:: https://img.shields.io/pypi/v/clickmodoro.svg
        :target: https://pypi.python.org/pypi/clickmodoro

.. image:: https://img.shields.io/travis/bloodpet/clickmodoro.svg
        :target: https://travis-ci.org/bloodpet/clickmodoro

.. image:: https://readthedocs.org/projects/clickmodoro/badge/?version=latest
        :target: https://clickmodoro.readthedocs.io/en/latest/?badge=latest
        :alt: Documentation Status


.. image:: https://pyup.io/repos/github/bloodpet/clickmodoro/shield.svg
     :target: https://pyup.io/repos/github/bloodpet/clickmodoro/
     :alt: Updates



Pomodoro


* Free software: GNU General Public License v3
* Documentation: https://clickmodoro.readthedocs.io.


Features
--------

* TODO

Credits
-------

This package was created with Cookiecutter_ and the `audreyr/cookiecutter-pypackage`_ project template.

.. _Cookiecutter: https://github.com/audreyr/cookiecutter
.. _`audreyr/cookiecutter-pypackage`: https://github.com/audreyr/cookiecutter-pypackage
